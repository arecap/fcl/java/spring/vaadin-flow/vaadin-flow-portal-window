package com.vaadin.flow.spring.portalwindow;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.eventbus.EventBus;
import org.springframework.cop.support.BeanUtil;

import java.util.EventObject;

@Deprecated
public class CollapsibleEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public CollapsibleEvent(Component source) {
        super(source);
    }

    public static void push(Component source) {
        BeanUtil.getBean(EventBus.class).push(new CollapsibleEvent(source));
    }

}
