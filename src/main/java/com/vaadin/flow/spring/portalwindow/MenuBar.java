package com.vaadin.flow.spring.portalwindow;


import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class MenuBar extends VerticalLayout implements ComponentSetup {

	@Autowired
	private DynamicMenuBar dynamicMenuBar;

	@Autowired
	private StaticMenuBar staticMenuBar;

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSpacing(false);
        setHeightFull();
        setWidth("65px");
        getStyle().set("background-color", "#264c6c");
        getStyle().set("position", "relative");

        add(dynamicMenuBar, staticMenuBar);
    }


}
