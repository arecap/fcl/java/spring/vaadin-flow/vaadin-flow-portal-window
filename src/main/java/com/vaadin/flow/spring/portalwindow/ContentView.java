package com.vaadin.flow.spring.portalwindow;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class ContentView extends HorizontalLayout implements ComponentSetup {

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSpacing(false);
        setSizeFull();
        getStyle().set("position", "relative");
        getStyle().set("margin", "0px");
        getStyle().set("border-left", "1px solid black");
    }

}
