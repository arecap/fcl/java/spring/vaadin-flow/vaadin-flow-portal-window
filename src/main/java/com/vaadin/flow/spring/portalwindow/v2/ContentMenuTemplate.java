package com.vaadin.flow.spring.portalwindow.v2;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringComponent
@UIScope
public class ContentMenuTemplate extends HorizontalLayout implements ComponentSetup {

    private List<VerticalLayout> slices = new ArrayList<>();

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSpacing(false);
        setHeightFull();
        getStyle().set("position", "relative");
    }

    public int addSlice() {
        VerticalLayout verticalLayout = constructSlice();
        add(verticalLayout);
        slices.add(verticalLayout);
        return slices.size();
    }

    public VerticalLayout addSliceAtIndex(int index) {
        VerticalLayout verticalLayout = constructSlice();
        addComponentAtIndex(index, verticalLayout);
        slices.add(index, verticalLayout);
        return verticalLayout;
    }

    public int removeSlice(int index) {
        Optional<VerticalLayout> slice = getSliceAtIndex(index);
        if(slice.isPresent()) {
            remove(slice.get());
            slices.remove(slice.get());
        }
        return slices.size();
    }

    public int getSlicesCount() {
        return slices.size();
    }

    public Optional<VerticalLayout> getSliceAtIndex(int index) {
        return index >= 0 && index < slices.size() ? Optional.of(slices.get(index)) : Optional.empty();
    }


    private VerticalLayout constructSlice() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setPadding(false);
        verticalLayout.setSpacing(false);
        verticalLayout.setHeightFull();
        verticalLayout.setWidth("350px");
        verticalLayout.getStyle().set("border-left", "1px solid black");
        verticalLayout.getStyle().set("margin", "0px");
        verticalLayout.getStyle().set("position", "relative");
        return verticalLayout;
    }

}
