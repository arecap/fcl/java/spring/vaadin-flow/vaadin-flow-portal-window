package com.vaadin.flow.spring.portalwindow;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(BootPortalWindow.class)
@Documented
public @interface EnablePortalWindow {
}
