package com.vaadin.flow.spring.portalwindow.v2;


import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@Tag("pw-menu-bar")
@HtmlImport("frontend://portalwindow/MenuBar.html")
public class MenuBarTemplate extends PolymerTemplate<TemplateModel> implements ComponentSetup {

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuBarTemplate.class);

    @Id("dynamicMenuBar")
	private VerticalLayout dynamicMenuBar;

    @Id("staticMenuBar")
	private VerticalLayout staticMenuBar;


    @PostConstruct
    public void setup() {
        System.out.println("postConstruct:\t"  + getClass());
    }

    public VerticalLayout getDynamicMenuBar() {
        return dynamicMenuBar;
    }

    public VerticalLayout getStaticMenuBar() {
        return staticMenuBar;
    }
}
