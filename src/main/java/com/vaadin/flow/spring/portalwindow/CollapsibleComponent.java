package com.vaadin.flow.spring.portalwindow;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@Deprecated
public class CollapsibleComponent extends Div implements ComponentSetup {

	@Autowired
	private MenuBar menuBar;

	private HorizontalLayout mainArea;
	
	private VerticalLayout firstSlice;
	
	private VerticalLayout secondSlice;
	
	private VerticalLayout thirdSlice;
	
	private VerticalLayout fourthSlice;
	
	private VerticalLayout rightPart;

    private Div collapseButton;
    
    private Div collapseButton2;
    
    private boolean collapsed = false;
    
    private boolean collapsed2 = false;
    
    private int lastWidth;
    
    private int width = 300;
    private int extendedWidth = width+550;
    private int paddingWidth = 0;
    
    private boolean componentVisible;
    
    private boolean isLeftSideActive;
    
    private boolean isRightSideActive;

    private void constructWrapper() {
        mainArea = new HorizontalLayout();
        mainArea.getStyle().set("position", "absolute");
//        mainArea.getStyle().set("display", "block");
        mainArea.getStyle().set("overflow", "auto");
        mainArea.getStyle().set("top", "0px");
        mainArea.getStyle().set("left", paddingWidth+"px");//"0px"
        mainArea.getStyle().set("width", (paddingWidth+width)+"px");//"305px");
        mainArea.getStyle().set("bottom", "0px");
//        mainArea.getStyle().set("box-shadow", "0 0 20px rgba(0, 0, 0, 0.3)");
        mainArea.getStyle().set("background-color", "#eee");
        buildSlices(mainArea);
        add(mainArea);
        setLeftSliceActive(false);
    }

    private void setLeftBorder(Component comp) {
    	comp.getElement().getStyle().set("border-left", "1px solid black");
    }
    
    private void setBottomBorder(Component comp) {
    	comp.getElement().getStyle().set("border-bottom", "1px solid black");
    }
    
    private void buildSlices(HorizontalLayout mainArea) {
    	buildSlice(mainArea, firstSlice = new VerticalLayout());
    	firstSlice.getStyle().set("width", (paddingWidth+width)+"px");//"305px");
    	setLeftBorder(mainArea);
    	
    	buildSlice(mainArea, rightPart = new VerticalLayout());
    	rightPart.setWidthFull();
    	setLeftBorder(rightPart);
    	
    	HorizontalLayout top;
    	buildSlice(rightPart, top = new HorizontalLayout());
    	setBottomBorder(top);
    	top.setWidthFull();
    	
    	VerticalLayout bottom;
    	buildSlice(rightPart, bottom = new VerticalLayout());
    	bottom.setWidthFull();
    	
    	buildSlice(top, secondSlice = new VerticalLayout());
    	
    	buildSlice(top, thirdSlice = new VerticalLayout());
    	setLeftBorder(thirdSlice);
    	thirdSlice.setPadding(true);
//    	setBottomBorder(thirdSlice);
    	
    	buildSlice(bottom, fourthSlice = new VerticalLayout());
//    	fourthSlice.getElement().getStyle().set("border-top", "1px solid black");
    	fourthSlice.setPadding(true);
	}

    private void buildSlice(HorizontalLayout parent, HorizontalLayout slice) {
//		slice.setWidthFull();
		slice.setPadding(false);
		parent.add(slice);
    }
    
    private void buildSlice(VerticalLayout parent, HorizontalLayout slice) {
//		slice.setWidthFull();
		slice.setPadding(false);
		parent.add(slice);
    }
    
	private void buildSlice(HorizontalLayout parent, VerticalLayout slice) {
//		slice.setWidthFull();
		slice.setPadding(false);
		parent.add(slice);
    }
	
	private void buildSlice(VerticalLayout parent, VerticalLayout slice) {
//		slice.setWidthFull();
		slice.setPadding(false);
		parent.add(slice);
    }
	
	private void constructCollapseWrapper() {
        collapseButton = new Div();
        collapseButton.getStyle().set("position", "absolute");
        collapseButton.getStyle().set("display", "block");
        collapseButton.getStyle().set("background-color", "#cccccc");
        collapseButton.getStyle().set("bottom", "70px");
        collapseButton.getStyle().set("left", (paddingWidth)+"px");//"305px");
        collapseButton.getStyle().set("width", "10px");//"12px");
        collapseButton.getStyle().set("height", "30px");
        collapseButton.getStyle().set("padding", "10px");
        collapseButton.add(VaadinIcon.ANGLE_LEFT.create());
        collapseButton.addClickListener(clickEvent -> {
            if(/*mainArea.getStyle().get("display").equalsIgnoreCase("block")*/!collapsed) {
                collapse();
            } else {
                expand();
            }
        });
        add(collapseButton);
    }
	
	private void constructCollapseWrapper2() {
		collapseButton2 = new Div();
		collapseButton2.getStyle().set("position", "absolute");
		collapseButton2.getStyle().set("display", "block");
		collapseButton2.getStyle().set("background-color", "#cccccc");
		collapseButton2.getStyle().set("bottom", "30px");
		collapseButton2.getStyle().set("left", (paddingWidth)+"px");//"305px");
		collapseButton2.getStyle().set("width", "10px");//"12px");
		collapseButton2.getStyle().set("height", "30px");
		collapseButton2.getStyle().set("padding", "10px");
		collapseButton2.add(VaadinIcon.ANGLE_DOUBLE_RIGHT.create());
		collapseButton2.addClickListener(clickEvent -> {
            if(/*mainArea.getStyle().get("display").equalsIgnoreCase("block")*/!collapsed2) {
                collapse2();
            } else {
                expand2();
            }
        });
        add(collapseButton2);
    }

//    @PostConstruct
//    public void init(HasComponents route) {
//        wrapper.removeAll();
//        route.add(this);
//    }

    public void collapse() {
    	collapsed = true;
//        mainArea.getStyle().set("display", "none");
    	mainArea.setVisible(false);
        collapseButton.getStyle().set("left", (paddingWidth)+"px");//"0px");
        collapseButton.removeAll();
        collapseButton.add(VaadinIcon.ANGLE_RIGHT.create());
    }

    public void expand() {
    	collapsed = false;
//        mainArea.getStyle().set("display", "initial");
        mainArea.setVisible(true);
        collapseButton.getStyle().set("left", (paddingWidth+lastWidth)+"px");//"305px");
        collapseButton.removeAll();
        collapseButton.add(VaadinIcon.ANGLE_LEFT.create());
    }
    
    public void collapse2() {
    	collapsed2 = true;
//        mainArea.getStyle().set("display", "none");
    	mainArea.setVisible(false);
        collapseButton2.getStyle().set("left", (paddingWidth)+"px");//"0px");
        collapseButton2.removeAll();
        collapseButton2.add(VaadinIcon.ANGLE_DOUBLE_RIGHT.create());
    }

    public void expand2() {
    	collapsed = false;
//        mainArea.getStyle().set("display", "initial");
        mainArea.setVisible(true);
        collapseButton2.getStyle().set("left", (paddingWidth+lastWidth)+"px");//"305px");
        collapseButton2.removeAll();
        collapseButton2.add(VaadinIcon.ANGLE_DOUBLE_LEFT.create());
    }

    protected HorizontalLayout getMainArea(){
    	return mainArea;
    }

	@PostConstruct
	public void setup() {
		paddingWidth = Integer.parseInt(menuBar.getWidth().substring(0,menuBar.getWidth().indexOf("px")));
		
//		constructCollapseWrapper2();
		constructCollapseWrapper();
		constructWrapper();
		
//		collapse();
	}

	public boolean isComponentVisible() {
		return componentVisible;
	}

	public void setComponentVisible(boolean componentVisible) {
		this.componentVisible = componentVisible;
	}

	public void addToFirstSlice(Component components) {
		firstSlice.add(components);
//		mainArea.getStyle().set("width", (paddingWidth+width)+"px");//"305px");
//		collapseButton.getStyle().set("left", (2*paddingWidth+width)+"px");//"305px");
	}
	
	public void addToFirstSlice(int index, Component component) {
		firstSlice.addComponentAtIndex(index, component);
//		mainArea.getStyle().set("width", (paddingWidth+width)+"px");//"305px");
//		collapseButton.getStyle().set("left", (2*paddingWidth+width)+"px");//"305px");
	}

	public void addToSecondSlice(Component... components) {
		secondSlice.add(components);
//		mainArea.getStyle().set("width", (paddingWidth+extendedWidth)+"px");//"305px");
//		collapseButton.getStyle().set("left", (2*paddingWidth+extendedWidth)+"px");//"305px");
	}
	
	public void addToThirdSlice(Component... components) {
		thirdSlice.add(components);
//		setWidth(paddingWidth+width+extendedWidth);
	}
	
	public void addToForthSlice(Component... components) {
		fourthSlice.add(components);
		setWidth(paddingWidth+width+extendedWidth);
	}
	
	private void setWidth(int value) {
		lastWidth = value;
		mainArea.getStyle().set("width", (value)+"px");//"305px");
		collapseButton.getStyle().set("left", (paddingWidth+value)+"px");//"305px");
	}
	
	public void setLeftSliceActive(boolean value) {
		isLeftSideActive = value;
		if (!value) {
			collapse();
		}else {
			if (isRightSideActive) {
				setRightSliceActive(isRightSideActive);
			}
			expand();
		}
	}
	
	public void setRightSliceActive(boolean value) {
		isRightSideActive = value;
		if (value) {
			setWidth(paddingWidth+width+extendedWidth);;
		}else {
			setWidth(paddingWidth+width);
		}
		rightPart.setVisible(value);
	}
}
