package com.vaadin.flow.spring.portalwindow;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.core.annotation.AnnotatedElementUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@SpringComponent
@UIScope
@Deprecated
public class NavBarTabNavigationWindow extends Div implements ComponentSetup {

    private Tabs selectorTabs = new Tabs();

    private Map<Tab, String> tabsToUrl = new HashMap<>();

    @PostConstruct
    public void setup() {
        setHeightFull();
//        getStyle().set("max-width", "50%");
//        getStyle().set("float","left");
//        selectorTabs.setSizeFull();
        selectorTabs.addThemeVariants(TabsVariant.LUMO_SMALL);
        selectorTabs.addSelectedChangeListener(event -> {
            String url = tabsToUrl.get(selectorTabs.getSelectedTab());
            if (url != null && (UI.getCurrent().getInternals().getLastHandledLocation() == null ||
                    !UI.getCurrent().getInternals().getLastHandledLocation().getPath().contains(url))) {
                UI.getCurrent().getPage().executeJavaScript("window.location.replace(\"" + url + "\");");
            }
        });
        selectorTabs.setOrientation(Orientation.VERTICAL);
        add(selectorTabs);
        addTabs();
    }

    private void addTabs() {
        RouteConfiguration.forApplicationScope().getAvailableRoutes().stream().forEach(routeData -> {
            NavBarTabWindow navBarTab = AnnotatedElementUtils.findMergedAnnotation(routeData.getNavigationTarget(), NavBarTabWindow.class);
            if(navBarTab != null) {
                addTab(navBarTab.value(), routeData.getUrl());
            }
        });
    }

    private void addTab(String label, String url) {
        Tab tab = new Tab(label);
        selectorTabs.add(tab);
        tabsToUrl.put(tab, url);
        if(UI.getCurrent().getInternals().getLastHandledLocation().getPath().contains(url)) {
            selectorTabs.setSelectedTab(tab);
        } else {
            tab.getStyle().set("color", "#cccccc");
        }
    }

}
