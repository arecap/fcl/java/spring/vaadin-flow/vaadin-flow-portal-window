package com.vaadin.flow.spring.portalwindow.v2;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.templatemodel.TemplateModel;


@Tag("portal-window-template")
@HtmlImport("frontend://portalwindow/PortalWindowTemplate.html")
@Uses(ModalMenuTemplate.class)
@Uses(MenuBarTemplate.class)
@Uses(ContentTemplate.class)
public class PortalWindowTemplate extends PolymerTemplate<TemplateModel> implements RouterLayout {


}
