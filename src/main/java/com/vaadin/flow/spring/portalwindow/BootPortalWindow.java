package com.vaadin.flow.spring.portalwindow;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class BootPortalWindow {
}

