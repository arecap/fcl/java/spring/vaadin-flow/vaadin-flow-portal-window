package com.vaadin.flow.spring.portalwindow;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PortalWindow {
}
