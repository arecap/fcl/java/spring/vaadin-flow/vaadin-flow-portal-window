package com.vaadin.flow.spring.portalwindow;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class AbsoluteResizeableContentOption extends HorizontalLayout implements ComponentSetup {

    @Autowired
    private ContentOption contentOption;

    private Div resizer = new Div();

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSpacing(false);
        getStyle().set("position", "absolute");
        getStyle().set("background", "white");
        getStyle().set("z-index", "100");
        getStyle().set("top", "0px");
        getStyle().set("bottom", "0px");
        getStyle().set("border-right", "1px solid black");
        setupResizer();
    }

    public void show() {
        if(contentOption.getSlicesCount() == 0) {
            getStyle().set("left", "0px");
            contentOption.add(this);
        } else {
            VerticalLayout slice = contentOption.getSliceAtIndex(contentOption.getSlicesCount() - 1).get();
            getStyle().set("left", slice.getStyle().get("width"));
            slice.add(this);
        }
        UI.getCurrent().getPage()
                .executeJavaScript("$0.addEventListener('mousedown', function(e) {\n" +
                                "      e.preventDefault()\n" +
                                "      window.addEventListener('mousemove', resize)\n" +
                                "      window.addEventListener('mouseup', stopResize)\n" +
                                "    }); " + getJsResizeFn() + getJsStopResizeFn(),
                        resizer.getElement(), this.getElement());
    }

    public void hide() {
        if(contentOption.getSlicesCount() == 0) {
            contentOption.remove(this);
        } else {
            contentOption.getSliceAtIndex(contentOption.getSlicesCount() - 1).get().remove(this);
        }
    }

    public void reset() {
        UI.getCurrent().getPage()
                .executeJavaScript("$0.style.width = null;", this.getElement());
    }

    private void setupResizer() {
        resizer.setWidth("20px");
        resizer.setHeight("20px");
        resizer.getStyle().set("position", "absolute");
        resizer.getStyle().set("background",  "white");
        resizer.getStyle().set("border-radius",  "50%");
        resizer.getStyle().set("border", "3px solid #4286f4");
        resizer.getStyle().set("cursor", "nwse-resize");
        resizer.getStyle().set("right", "-13px");
        resizer.getStyle().set("top", "30px");
        add(resizer);
    }


    private String getJsResizeFn() {
        return "function resize(e) {\n" +
                "   \t$1.style.width = e.pageX - $1.getBoundingClientRect().left + 'px'\n" +
                "}";
    }

    private String getJsStopResizeFn() {
        return "    function stopResize() {\n" +
                "       \twindow.removeEventListener('mousemove', resize)\n" +
                "    }";
    }

}
