package com.vaadin.flow.spring.portalwindow;


import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class StaticMenuBar extends VerticalLayout implements ComponentSetup {

    @PostConstruct
    public void setup() {
        setPadding(false);
        setWidthFull();
        getStyle().set("margin-bottom", "20px");
    }

}
