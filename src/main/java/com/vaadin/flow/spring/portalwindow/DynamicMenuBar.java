package com.vaadin.flow.spring.portalwindow;


import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class DynamicMenuBar extends VerticalLayout implements ComponentSetup {

//	@Autowired
//	public MenuBar menuBar;

    @PostConstruct
    public void setup() {
        setPadding(false);
//        setWidth(((VerticalLayout)this.getParent().get()).getWidth());
        setWidthFull();
        setHeightFull();
//        setHeight("60px");
//        getStyle().set("position", "relative");
//        getStyle().set("display", "block");
//        getStyle().set("bottom", "0px");
    }

}
