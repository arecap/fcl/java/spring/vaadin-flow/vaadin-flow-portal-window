package com.vaadin.flow.spring.portalwindow;


import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class PortalContent extends HorizontalLayout implements ComponentSetup {

    @Autowired
    private ContentOption contentOption;

    @Autowired
    private ContentView contentView;

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSizeFull();
        add(contentOption, contentView);
    }

}
