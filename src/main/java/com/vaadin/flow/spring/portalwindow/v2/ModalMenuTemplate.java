package com.vaadin.flow.spring.portalwindow.v2;


import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ComponentSetup;
import com.vaadin.flow.templatemodel.TemplateModel;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@HtmlImport("frontend://portalwindow/ModalMenu.html")
@Tag("pw-modal-menu")
public class ModalMenuTemplate extends PolymerTemplate<TemplateModel> implements ComponentSetup {

    @Id("pw-modal-menu-bar")
    private VerticalLayout menuBar;

    @Id("pw-modal-menu-content")
    private HorizontalLayout menuContent;

    @PostConstruct
    public void setup() {
//        removeFromParent();
        System.out.println("postConstruct:\t"  + getClass());
    }

    public void open(Component component) {
        if(getParent().isPresent()) {
            getParent().get().getElement().appendChild(this.getElement());
        }
        menuContent.add(component);
    }

    @EventHandler
    public void close() {
        menuContent.removeAll();
        removeFromParent();
    }

    private void removeFromParent() {
        if(getParent().isPresent()) {
            getParent().get().getElement().removeChild(this.getElement());
        }
    }

    private void setupMenuBar() {
        Button closeBtn = new Button(VaadinIcon.ARROW_LONG_LEFT.create());
        closeBtn.setWidthFull();
        closeBtn.addClickListener(this::handleClose);
        menuBar.add(closeBtn);
    }

    private void handleClose(ClickEvent<Button> buttonClickEvent) {
        close();
    }


}
