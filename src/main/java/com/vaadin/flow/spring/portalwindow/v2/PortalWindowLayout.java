package com.vaadin.flow.spring.portalwindow.v2;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RouterLayout;


@ParentLayout(PortalWindowTemplate.class)
public class PortalWindowLayout extends HorizontalLayout implements RouterLayout {


}
