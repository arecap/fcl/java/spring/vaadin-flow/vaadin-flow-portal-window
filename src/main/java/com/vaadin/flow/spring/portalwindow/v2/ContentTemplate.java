package com.vaadin.flow.spring.portalwindow.v2;


import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.portalwindow.ContentOption;
import com.vaadin.flow.spring.portalwindow.ContentView;
import com.vaadin.flow.spring.template.ComponentSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
@Tag("pw-content")
public class ContentTemplate extends HorizontalLayout implements ComponentSetup {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentTemplate.class);

    @Autowired
    private ContentOption contentOption;

    @Autowired
    private ContentView contentView;

    @PostConstruct
    public void setup() {
        System.out.println("postConstruct:\t"  + getClass());
        setPadding(false);
        setSizeFull();
        add(contentOption, contentView);
    }

}
