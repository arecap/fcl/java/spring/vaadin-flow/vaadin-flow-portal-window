package com.vaadin.flow.spring.portalwindow;


import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.template.ComponentSetup;
import com.vaadin.flow.spring.template.annotation.GuiController;
import org.contextualj.lang.annotation.expression.SourceAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

@GuiController
@SourceAnnotation(PortalWindow.class)
@Order(value= Ordered.HIGHEST_PRECEDENCE)
@Deprecated
public class RouteWrapper extends HorizontalLayout implements ComponentSetup {

    @Autowired
    private MenuBar menuBar;
    
//    @Autowired
//    private CollapsibleComponent collapsibleComponent;

    @Autowired
    private PortalContent portalContent;

    @PostConstruct
    public void setup() {
        setPadding(false);
        setSpacing(false);
        setSizeFull();
        add( menuBar, portalContent);
    }

//    @ViewMapping
    public void templateRoute(HasComponents route) {
        Assert.isAssignable(HasSize.class, route.getClass());
        ((HasSize)route).setSizeFull();
        route.add(this);
    }


}
